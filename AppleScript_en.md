## Supported commands
Now you can call PD Runner through AppleScript. PD Runner supports the following commands:  
`start vm "name" [with password "pass"]`, `start macvm`, `list all`, `start all`, `stop all`, `hide icon`, `show icon`

## Use example
👉**Run PD runner before using Applescript, or run:**  
```AppleScript
tell application "PD Runner" to activate
```  
---
Start a virtual machine named "windows 11":  
```AppleScript
tell application "PD Runner" to start vm "Windows 11"
```  
If the virtual machine is encrypted: 
```AppleScript
tell application "PD Runner" to start vm "Windows 11" with password "foobar"
```  
Start the macOS virtual machine based on Apple Silicon:  
```AppleScript
tell application "PD Runner" to start macvm
```  
List all virtual machines:  
```AppleScript
tell application "PD Runner" to list all
```  
Start/Stop all virtual machines:  
```AppleScript
tell application "PD Runner" to start all
tell application "PD Runner" to stop all
```  
Hide/Show menu bar icon (works at next time you start PD Runner):  
```AppleScript
tell application "PD Runner" to hide icon
tell application "PD Runner" to show icon
```  